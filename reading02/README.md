Reading 02
Brenna Leahy
bleahy1
==========

1. How would you run the script even though it is not executable?
	/bin/bash exists.sh
	This makes it both executable and readable. 

2. How would you make this script executable?
	chmod +x exists.h 
	THis would make the script executable. 

3. Once this script is executable, how would you run it directly?
	Just by typing in ./exists.sh

4. What is the purpose of the line #!/bin/sh?
	It's purpose is to say that the code is to be interpreted by a certain
	shell. In this case the bourne shell(sh).

5. What is the output of the script if you run it with the arguments *?
	$ exists.sh *?
	exists.sh exists!

6. What is the $1 that appears in the script?
	The $ marks a variable. So therefore the $1 is the first thing entered 
	into the command line. 

7. What does test -e "$1" do?
	It tests if the file located in position $1 exists. It does this because
	-e is the flag that says the file exists. 

8. What does this script do?
	If the variable $1 exists it prints "(the variable value) exists!" or it
	prints out "(the variable value) does not exist!"
	
