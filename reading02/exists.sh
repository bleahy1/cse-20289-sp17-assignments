#!/bin/sh

if [ "$#" -eq "0" ]; then # not printing out that README.md exists in the directory 
    echo "ERROR WAS ENCOUNTERED"  # print the value for the $1 variable 
	exit 1
fi

a=0

while [ "$#" -gt "0" ]
do 
	if [ -e "$1" ] ; then 
		echo "$1 EXISTS"
	else
		echo "$1 DOES NOT EXIST"
	fi
	
	`find $1 &> /dev/null` 
	
	if [ "$?" -ne "0" ] ; then 
	
		a=1; 
		
	fi 
	
	shift 
	
done 
exit $a

