Reading 03 - Grading
===================================
**Score**: 3.5/4

**Grader**: Mimi Chen

Deductions
--------------------
1) (-0) You only need one pipe 

4) (-0.25) You only want the root user shell, so you have to grep for a shell that **begins** with the root user with the following command:

    cat /etc/passwd | grep ^root | cut -d ':' -f 7

8) (-0.25) You should use the command **diff**. If you want to use **comm**, you should use the flag -3 instead of -23


Comments
--------------------
