#!/bin/sh


STATE='Indiana'
CITY='emptyString'
FORMAT='noFormat' 

getCity() { grep "$CITY" | sed -En 's/.*([0-9]{5}).*/\1/p'
}

getFormat() { if [ "$FORMAT" == "csv" ] ; then paste -d "," -s  
fi
}



while getopts ":hc:f:s:" options; do
	
	case $options in
		h)  echo "Usage: zipcode.sh"
			echo "-c      CITY    Which city to search"
    		echo "-f      FORMAT  Which format (text, csv)"
    		echo "-s      STATE   Which state to search (Indiana)"
    
    		echo "If not CITY is specified, then all the zip codes for the STATE are displayed."
    
			exit 1
			;;
		c) copt="$1"
			CITY=$OPTARG
			#shift
			;;
		f) fopt="$1"
			FORMAT=$OPTARG
			#shift
			;;
		s) sopt="$1"
			STATE=$OPTARG
			#shift
			;;
		*) echo"invalid flag";;
	esac
done

if [ ! $# -gt 0 ] 
then 
	curl -s "http://www.zipcodestogo.com/$STATE/" | grep -Eo "[0-9]{5}/" | cut -d "/" -f1 

elif [ "$FORMAT" != "noFormat" ] ; then 
	curl -s "http://www.zipcodestogo.com/$STATE/" | grep -Eo "[0-9]{5}/" | cut -d "/" -f1 | getFormat
	
elif [ ! -z $copt ] ; then 
	#echo "$CITY $STATE"
	curl -s "http://www.zipcodestogo.com/$STATE/" | grep "$CITY/" | grep -Eo "[0-9]{5}/" | cut -d "/" -f1

elif [ ! -z $sopt ] ; then 
	#echo "$STATE"
	curl -s "http://www.zipcodestogo.com/$STATE/" | grep -Eo "[0-9]{5}/" | cut -d "/" -f1  
	
  
fi



exit 1
