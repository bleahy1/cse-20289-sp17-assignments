Activity #1 Caesar.sh

1. How you parsed the command line arguments.
	______________________________________________________
	
2. How you constructed the source set (ie. SET1).
	The source set is represented by the empty quotes and the a-z and A-Z.
	
3. How you constructed the target set (ie. SET2).
	THe target set is made with the '\001' deliminator. As well as the "a-za-z" and the
	"A_ZA_Z".
	
4. How you used both of these sets to perform the encryption.
	Switch is the variable that rotates the letters, if it doesn't receive an input it
	automatically uses 13.  Then the program prints out the phrase using printf on the
	switch function. Pad is then used to map the input to the switch function. It goes 
	to the tr command and calls the pad function, this is where the original charaqcter 
	is changed to the new rotated character keeping in mind the case of the character. 


Activity #2 Broify.sh

1. How you parsed the command line arguments.
	____________________________________________________
	
2. How you removed comments.
	Sed was used to remove the comments. Sed finds things that match the input delimiter, I
	used the delimiter #. THen replaced all lines that start with # with nothing.
	
3. How you removed empty lines.
	Searches for a line of tabs and then gets rid of it deleting the line.

4. How the command line options affected your operations above.
	WHen the w flag is given the empty lines in the code will still show.  If the d flag is 
	given then the comments will be deleted. 


Activity #3 zipcode.sh

1. How you parsed the command line arguments.
	I parsed the command line with getops and $#.
	
2. How you extracted the zip codes.
	I extracted the zipcodes using curl and grep. 
	
3. How you filtered by STATE and CITY.
	I don't think I accomplished this correctly. But I used grep and sed.
	
4. How you handled the text and csv FORMAT options.
	I know for a fact I didn't get this to work correctly. 

