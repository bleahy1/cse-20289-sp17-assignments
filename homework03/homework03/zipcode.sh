#!/bin/sh


STATE='Indiana'

if [ $# -gt 0 ]
then 
	curl -s "http://www.zipcodestogo.com/$STATE/" | grep -Eo "[0-8]{5}" 
	
fi

getCity() { grep "$CITY" | sed -En 's/.*([0-9]{5}).*/\1/p'
}

getFormat() { if [ "$format" = "csv" ] ; then tr '\n' ',' | sed 's/.$//'
fi
}


while getopts ":hcfs:" options; do
	case $options in
		h)  echo "Usage: zipcode.sh"
			echo "-c      CITY    Which city to search"
    		echo "-f      FORMAT  Which format (text, csv)"
    		echo "-s      STATE   Which state to search (Indiana)"
    
    		echo "If not CITY is specified, then all the zip codes for the STATE are displayed."
    
			exit 1
			;;
		c) copt=$OPTARG
			CITY="$1"
			shift
			;;
		f) fopt=$OPTIND
			FORMAT="$1"
			shift
			;;
		s) sopt=$OPTIND
			STATE="$1"
			shift
			;;
		*) echo"invalid flag";;
	esac
done


if [ "$#" -gt "2" ] ; then 
	curl -s "http://www.zipcodestogo.com/$STATE/" | grep -Eo "[0-9]{1,5}" | getFormat
fi

exit 1
