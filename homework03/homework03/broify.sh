#!/bin/sh

if ["$1"= "-h" ]; then 
	echo "Usage: broify.sh"
	echo "-d DELIM    Use this as the comment delimiter."
	echo "-W          Don't strip empty lines."
	
	exit 1
fi

doption="#"
while getopts d:W options; do       # getopts parses until first non-option
  case $options in
    d)
      doption=$OPTARG;;             # Sets doption to whatever is found by getopts
    W)
      woption=1;;
    *)
      echo "Invalid entry:";;
  esac
done

shift $((OPTIND-1))              # Optind starts at 1 and acts as the index of when reset to 1 it can parse again.


if [[ ! -z $woption ]]; then
  sed "s:$doption.*$::g; s/[ \t]*$//"
else
  sed "s:$doption.*$::g; s/[ \t]*$//; /^$/d"
fi
