#!/bin/sh

if ["$1"= "-h" ]; then 
	echo "Usage: caesar.sh [rotation]"
	echo "This program will read from stdin and rotate (shift right) the text by the specified rotation. If none is specified, then the default value is 13."
	
	exit 1
fi

switch=$((${1:-13} % 26))

pad=$(printf "%${switch}s" "" | tr ' ' '\001')

tr "${pad}a-z" "a-za-z" |

tr "${pad}A-Z" "A-ZA-Z"

