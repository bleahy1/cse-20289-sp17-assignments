Deductions:
    -1.5 caesar.sh doesn't run tests and code is mostly incorrect
    -1.0 broify.sh doesn't run test, code looks mostly okay
    -1.0 zipcode.sh doesn't run tests, if I should have graded 2 just slack me
    -1.0 README answers are either too short or incorrect

Comments:

Final Score: 10.5/15.0
Keep up the hard work
