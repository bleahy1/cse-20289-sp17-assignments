Reading 01
Brenna Leahy 
bleahy1
==========

1. Given the following command:   $ du -h /etc/ 2> /dev/null | sort -h > output.txt

	a. What is the purpose of the |?
		Uses the stdout of the first command as the stdin of the next command. 

	b. What is the purpose of the 2> /dev/null?
		2> Redirects standard error. Since the machine references input, output, and error as 0,1,and 2 the shell wants the number to redirect the files.
		
	c. What is the purpose of the > output.txt?
		This saves the standard output of the program into a file named output.txt.

	d. What is the purpose of the -h flags?
		THe -h puts it in human readable form. 

	e. Is the following command the same as one above? Explain: 
		$ du -h /etc/ | sort -h > output.txt 2> /dev/null
		No it isn't the same as above. THis command sends the stderror (2>) to /dev/null which does nothing to it. 
		

2. Given the following output of ls:	
		
	a. How would you concatenate all the files from 2002 into one file?
		cat 2002-0* > 2002File 

	b. How would you concatenate all the files from the month of December into one file?
		cat 200[2-6]-12 > december

	c. How would you view the contents of all the files from the month of January to June?
		cat 200{2..6}-[0]{1..6}

	d. How would you view the contents of all the files with an even year and odd month?
		cat 200{2,4,6}-{01,03,05,07,09,11}

	e. How would you view the contents of all the files from 2002 to 2004 in the months of September to December?
		cat 200[2-6]-{09..12}
		
		
3. Given the following output of ls -l:
		-rw------- 1 pbui users 0 Jan 18 07:19 Beastie
		-rwxr-xr-x 1 pbui users 0 Jan 18 07:18 Huxley
		-rwxr-x--- 1 pbui users 0 Jan 18 07:18 Tux

	a. Which files are executable by the owner?
		Huxley and Tux are executable by the owner.

	b. Which files are readable by members of the users group?
		Huxley and Tux are readable by the users group.

	c. Which files are writable by anyone (ie. the world)?
		Huxley is writable by anyone. 

	d. What command would you execute so that Tux had the same permissions as Huxley?
		chmod u= rwx, g=rx, o=rx Tux
		chmod 755 Tux

	e. What command would you execute so that Tux had the same permissions as Beastie?
		chmod 600 Tux
		
	
4. Suppose you run the bc command: $ bc

	a. How would you suspend the bc process?
		Ctrl +z
		
	b. How would you resume the suspended bc process?
		bg or fg can be used. bg resumes a process in the background whereas fg resumes a process in the foreground.

	c. How would you indicate the end of input to the bc process?
		Ctrl d	

	d. If the bc process was still running, how would you terminate the bc process?
		kill -9 [pid]. The Pid is found with the ps command.

	e. If the bc process was still running, how would you terminate the bc process from another terminal?
		
	
	
	
	
	

