Homework 01
===========dffg

Activity 1 (2pts)

1.  a)  I only have two folders because I was told the third folder is an NDcampus folder and since I am a Saint Mary's student I don't have it. However there are two folders and one has the administrator who has full permissions, the other has me who has full permissions, and then there is anyuser who only has permissions to lookup.
	b) It is a networking ACL that determines which directories are private and which are public. 
	
2.  a) The result of the command was as follows: /afs/nd.edu/web/bleahy1.txt: Permission denied. No I could not create the file because I didn't have acces to that specific location.
	b) I would say the unix permissions because acording to the ACLS I have permission to create the file.So therefore UNIx permissions are what are not allowing the file to be written there. 
	
Activity 2 (3pts)

1.	
| Command                             | Elapsed Time  |
|-------------------------------------|---------------|
| cp ...                              | 0.078 seconds |
| mv ...                              | 0.001 seconds |
| mv ...                              | 0.061 seconds |
| rm ...                              | 0 seconds     |

	a) With mv renaming its just changing the name its a simple process, Whereas mv woving and renaming has to move the file to a new location and then delete the old files which it must do one at a time. THat takes longer than a simple rename. 
	b) Since you are only deleting it, that should only take half of the time of the mv command which moves and then deletes.  
		
Activity 3 (3pts)	

1. bc < math.txt
2. bc < math.txt > results.txt
3. bc < math.txt > results.txt 2>1
4. cat math.txt | bc
	This is overkill because _______________________

Activity 4 (4pts)

1.grep "/sbin/nologin" "/etc/passwd" | wc -l    49
2. who | uniq | wc -l                           4 users 
3.du /etc/ 2> /dev/null | sort -rn | head -5
36828   /etc/
8556    /etc/gconf
7776    /etc/selinux
7756    /etc/selinux/targeted
7128    /etc/selinux/targeted/policy
4.  ps | grep "bash" | awk '{print $4}' | wc -l
1


Activity 5 (3pts)

1.  a) Commands that fail to terminate the program:
		All commands failed to do anything prior to hitting the hot key. I used ctrl z to 			suspend the process.
			
	b) Commands that terminate the program:
		Ctrl z , then to find the pid I typed in "ps x" which returned the pid which i 			used with the kill command by typing in "kill -9 15686".
		
2.  a)  ps x | grep "TROLL" | awk '{print $1}' | xargs kill
	b) killall -9 TROLL
		This tries to kill all the TROLL programs on the entire file system, but it won't kill everyone elses processes because I don't have permission. 

3.  kill -14 13784
	 Tell me I'm not dreaming but are we out of time?
	kill -10 
		All the things falling from the sky
		
	kill -12 
		Literally the entire star wars movie!!! 
		 


























3.



