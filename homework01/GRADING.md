Comments:

- 1.2.b: ACLs take precedence -0.5

- 3.3: `bc < math.txt > results.txt 2> /dev/null` -0.25

- 3.5: no answer -0.5

- 4.2: `who | cut -d ' ' -f 1 | sort | uniq | wc -l` -0.5

- 4.4: `ps aux` instead of `ps` -0.25

Score: 13/15
