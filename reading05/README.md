Reading 05
Brenna Leahy 
bleahy1
==========


1. Given the following Python script, echo.py:
	a). What is the purpose of import sys?
		This gives this python module access to codes in other modules. It also ensures that the code has the right libraries that it needs. It is similar to #include 

	b). Explain what for arg in sys.argv[1:]: does.
		This is the call for all of the command line arguments. 

	c). Why is there a trailing , in print arg,?
		the comma gives a space in between the arguments. 
		
2. Given the following Python script, cat.py:
	a). Explain how the command line arguments are parsed by discussing how
	   this loop while len(args) and args[0].startswith('-') and len(args
	   > 1 works.
	 		This line means that while the length of the args input isn't
	 		equal to zero and the first character of the args input is an
	 		- and the length of args is at least one then the program 
	 		will enter the while loop. 
	 		
	b). What are the purposes of the following code blocks:
		i). if len(args) == 0:
    			args.append('-')
    			
    		This command adds each argument to the list with a dash. 
    	
    	ii). if path == '-':
    			stream = sys.stdin
			 else:
    			stream = open(path)
    			
    		If the command line has a dash it will read in whatever the
    		flag is following the dash, otherwise if there is no dash it
    		will read in whatever is in the command line. 
    		
    	iii). line = line.rstrip()
    		
    		This gets rid of the whitespace.
    		
