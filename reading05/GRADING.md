Reading05 - Grading
===================

**Score**: 1.5/4

Deductions
----------
-1 late penalty
-.25 exists.py: does not exit with appropriate error status (non-zero on failure)
-.25 readme part1 q1b: the code snippet is how to iterate over each command line argument
-.25 readme part1 q1c: the trailing comma prevents a newline after each statement
-.25 readme part2 q3: rstrip() removes the newline at the end of the line and is necessary in this case because the print command adds a newline (so it prevents 2 newlines)
-.25 head.py: does not parse command line arguments properly
-.25 head.py: does not limit printing to specified number of items (no breaking out of loop)

Comments
--------
