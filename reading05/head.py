#!/usr/bin/env python2.7

import os
import sys
from sys import argv


# define usage

def usage(status=0):
    print '''Usage: {} files...

    -n NUM      print the first NUM lines instead of the first 10'''.format(os.path.basename(sys.argv[0]))
    sys.exit(status)

#parse command line 
data = sys.stdin.read()
args = sys.argv[1:]  
while len(args) and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    if arg == '-n':
        it = args[0]
    elif arg == '-h':
        usage(0)
    else:
        usage(1)

if len(args) == 0:
    args.append('-')



# Main execution

for path in args:
    if path == '-':
        stream = sys.stdin.read()
    else:
        sys.exit()
    for line in stream:
        line = line.rstrip()
        print stream

