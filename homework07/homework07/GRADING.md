Grading - Homework 08
=====================

Activity 1
----------

- 0.25  Compiler warnings
- 0.25  Makefile: objects depend on header

- 1.0   mostly fails test-list
- 2.5   did not complete msort

Activity 2
----------

- 0.5   mostly fails test
- 0.5   no benchmarking script

Total
-----

10 / 15
