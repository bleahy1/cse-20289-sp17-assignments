Homework 07
Brenna Leahy 
bleahy1
===========

Activity #1

1. Describe what memory you allocated in node_create and what memory you deallocated in node_delete. How did you handle the recursive flag?

	I overcompensated by allocating enough memory in node_create for a node that had enough memory for all of the node's information. node_delete deleted the memory that was alocated in node_create which included the node and everything in the node. The recursive flag sent the function into a while loop that would delete the entire list.

2. Describe what memory you allocated in list_create and what memory you deallocated in list_delete. How did you handle internal struct nodes?
	
	calloc( 1, sizeof(struct node)); I created a list that was big enough to hold numberous nodes. THis was what was deleted in list_delete. The internal structure was handled by creating a pointer and setting it to NULL. and then deleating each node as it iteerated through all of the nodes.  
	

3. Describe how your list_qsort function works and analyze its time complexity and space complexity in terms of Big-O notation (both average and worst case).
	
	It converts the list into an array and then sorts it, and then puts it back to a list. Making sure to free the array because it is only temporarally needed. The worst case complexity for space and time is O(n^2) and the average is O(nlog). 

4. Describe how your list_reverse function works and analyze its time complexity and space complexity in terms of Big-O notation (both average and worst case).
	
	This used recursion to set the head equal to the head and then set the head equal to the previous and next node to the current node, over and over.  The worst and average time and space complexity is O(n). 

5. Describe how your list_msort function works and analyze its time complexity and space complexity in terms of Big-O notation (both average and worst case).
	
	Didn't finish this one 
	
