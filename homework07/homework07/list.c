/* list.c */
/* Consulted Nohemi and she helped with debugging*/

#include "list.h"

/* Internal Function Prototypes */

static struct node*	reverse(struct node *curr, struct node *next);
static struct node *	msort(struct node *head, node_compare f);
static void		split(struct node *head, struct node **left, struct node **right);
static struct node *	merge(struct node *left, struct node *right, node_compare f);

/**
 * Create list.
 *
 * This allocates a list that the user must later deallocate.
 * @return  Allocated list structure.
 */
struct list *	list_create() {
    return calloc( 1, sizeof(struct node));
}

/**
 * Delete list.
 *
 * This deallocates the given list along with the nodes inside the list.
 * @param   l	    List to deallocate.
 * @return  NULL pointer.
 */
struct list *	list_delete(struct list *l) {
	struct node *nextN = NULL;
	
	for( struct node *nodeInUse = l -> head; nodeInUse; nodeInUse = nextN){
		nextN = nodeInUse -> next;
		node_delete(nodeInUse, false);
	}
	free(l);
	
return NULL;
}

/**
 * Push front.
 *
 * This adds a new node containing the given string to the front of the list.
 * @param   l	    List structure.
 * @param   s	    String.
 */
void		list_push_front(struct list *l, char *s) {
	l -> head = node_create (s, l -> head);
	(l -> size) ++;
	
	if( l -> head -> next == NULL){
		l -> tail = l -> head ;
	}
}

/**
 * Push back.
 *
 * This adds a new node containing the given string to the back of the list.
 * @param   l	    List structure.
 * @param   s	    String.
 */
void		list_push_back(struct list *l, char *s) {
	(l -> size) ++;
	struct node *tempN = node_create(s, NULL);
	
	if(l -> size > 1){
		l -> tail -> next = tempN; 
		l -> tail = tempN;
	}
	else{
		l -> tail = tempN;
		l -> head = tempN;
	}
}

/**
 * Dump list to stream.
 *
 * This dumps out all the nodes in the list to the given stream.
 * @param   l	    List structure.
 * @param   stream  File stream.
 */
void		list_dump(struct list *l, FILE *stream) {
	for ( struct node *nodeInUse = l -> head; nodeInUse; nodeInUse= nodeInUse -> next){
		node_dump(nodeInUse, stream);
	}
}

/**
 * Convert list to array.
 *
 * This copies the pointers to nodes in the list to a newly allocate array that
 * the user must later deallocate.
 * @param   l	    List structure.
 * @return  Allocate array of pointers to nodes.
 */
struct node **	list_to_array(struct list *l) {
	struct node **array = malloc( sizeof(struct node*) * l -> size);
	struct node *newN = l -> head;
	
	for( int i = 0; i < l -> size; i ++){
		array[i] = newN;
		newN= newN -> next;
	}
	
    return array;
}

/**
 * Sort list using qsort.
 *
 * This sorts the list using the qsort function from the standard C library.
 * @param   l	    List structure.
 * @param   f	    Node comparison function.
 */
void		list_qsort(struct list *l, node_compare f) {
	struct node **array = list_to_array(l);
	
	qsort(array, l -> size, sizeof( struct node*) , f);
	l -> head = array[0];
	l -> tail = array[(l -> size) - 1];
	
	for( int i ; i < l -> size - 1; i ++){
		array[i] -> next = array[i +1];
	}
	
	l -> tail -> next = NULL; 
	
	free( array);
}

/**
 * Reverse list.
 *
 * This reverses the list.
 * @param   l	    List structure.
 */
void		list_reverse(struct list *l) {
	l -> tail = l -> head;
	l -> head = reverse (l -> head, NULL);
	
}

/**
 * Reverse node.
 *
 * This internal function recursively reverses the node.
 * @param   curr    The current node.
 * @param   prev    The previous node.
 * @return  The new head of the singly-linked list.
 */
struct node*	reverse(struct node *curr, struct node *prev) {
	struct node *tempN;
	
	if ( curr -> next == NULL){
		curr -> next = prev; 
		return curr;
	}
	else {
		tempN = reverse ( curr -> next, curr);
		curr -> next = prev;
		return tempN;
	}
	
	return curr;

}

/**
 * Sort list using merge sort.
 *
 * This sorts the list using a custom implementation of merge sort.
 * @param   l	    List structure.
 * @param   f	    Node comparison function.
 */
void		list_msort(struct list *l, node_compare f) {
	return NULL;
}

/**
 * Performs recursive merge sort.
 *
 * This internal function performs a recursive merge sort on the singly-linked
 * list starting with head.
 * @param   head    The first node in a singly-linked list.
 * @param   f	    Node comparison function.
 * @return  The new head of the list.
 */
struct node *	msort(struct node *head, node_compare f) {
	return NULL;
}

/**
 * Splits the list.
 *
 * This internal function splits the singly-linked list starting with head into
 * left and right sublists.
 * @param   head    The first node in a singly-linked list.
 * @param   left    The left sublist.
 * @param   right   The right sublist.
 */
void		split(struct node *head, struct node **left, struct node **right) {
	
}

/**
 * Merge sublists.
 *
 * This internal function merges the left and right sublists into one ordered
 * list.
 * @param   left    The left sublist.
 * @param   right   The right sublist.
 * @param   f	    Node comparison function.
 * @return  The new head of the list.
 */
struct node *	merge(struct node *left, struct node *right, node_compare f) {

}
