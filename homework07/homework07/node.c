/* node.c */
/* Consulted Nohemi and she helped with debugging*/

#include "node.h"

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

/**
 * Create node. 
 *
 * This allocates and configures a node that the user must later deallocate.
 * @param   string      String value.
 * @param   next        Reference to next node.
 * @return  Allocated node structure.
 */
struct node *	node_create(char *string, struct node *next) {
    struct node *newNode = malloc( sizeof( struct node));
    
    if(newNode) {
        newNode -> string = strdup(string);
        newNode -> number = atoi(string);
        newNode -> next = next;
    }
    else{
        return NULL;
    }
    return newNode;
}

/**
 * Delete node.
 *
 * This deallocates the given node (recursively if specified).
 * @param   n           Node to deallocate.
 * @param   recursive   Whether or not to deallocate recursively.
 * @return  NULL pointer.
 */
struct node *   node_delete(struct node *n, bool recursive) {
    struct node *tempN;
    struct node *nodeP = n;
    tempN = nodeP -> next;
    
    free(nodeP -> string);
    free(nodeP);
    
    while (tempN != NULL && recursive){
        nodeP = tempN;
        tempN = nodeP -> next;
        free(nodeP -> string);
        free(nodeP);
    }    
    return NULL;
}

/**
 * Dump node to stream.
 * 
 * This dumps out the node structure (Node{string, number, next}) to the stream.
 * @param   n       Node structure.
 * @param   stream  File stream.
 **/
void            node_dump(struct node *n, FILE *stream) {
    if(n -> next == NULL){
        fprintf(stream, "node(%s, %d, %s) \n", n -> string , n -> number, "0x0");
    }
    else{
        fprintf(stream, "node(%s, %d, %p) \n", n -> string , n -> number, n -> next);
    }
}

/**
 * Compare node structures by number
 *
 * This compares two node structures by their number values.
 * @param   a       First node structure.
 * @param   b       Second node structure.
 * @return  < 0 if a < b, 0 if a == 0, > 0 if a > b
 */
int		node_compare_number(const void *a, const void *b) {
    
    struct node *intA = *(struct node **) a;
    struct node *intB = *(struct node **) b;
    
    return ((intA -> number) - (intB -> number));
}

/**
 * Compare node structures by string
 *
 * This compares two node structures by their string values.
 * @param   a       First node structure.
 * @param   b       Second node structure.
 * @return  < 0 if a < b, 0 if a == 0, > 0 if a > b
 */
int		node_compare_string(const void *a, const void *b) {

    struct node *stringA = *(struct node **) a;
    struct node *stringB = *(struct node **) b;
    
    return strcmp(stringA -> string, stringB -> string);
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
