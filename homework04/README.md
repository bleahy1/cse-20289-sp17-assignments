Homework 04
Brenna Leahy 
bleahy1
===========

blend.py

1. How you parsed command line arguments.
	I used a while loop that looked at the length of the arguments passed into it. If the
	argument started with a - then an if statement was entered to determine which global
	variable needed to be changed. 
	
2. How you managed the temporary workspace.
	I made a temporary workspace using the command, tempfile.mkdtemp(). I then made a cleanup
	function that removed the entire directory, this function was then called at the exit of
	the program so that when the program was over a message was printed and the directory was
	removed.
	
3. How you extracted the portrait URLS.
	I extracted the portrait URLs using request.get followed by the correct URL of the image
	that I desired with the correct netID in the URL. I used this function twice to get two 
	different images urls.

4. How you downloaded the portrait images.
	I did the exact same thing to get the images as to get the URLS. I used request.get but
	used the urls to get the image this time and then opened them in the temporary directory.

5. How you generated the blended composite images.
	I generated the blended images using the command os.system('composite -blend ....) This
	took the two images and blended them based on the index which was iterated based on the 
	step size of the program. 

6. How you generated the final animation.
	I used the command os.system( convert -loop ...) to make the final gif. This took the
	delay and the full list of images as a string to create the gif and saved it in the 
	third argument that was passed to it. 

7. How you checked for failure of different operations and exited early.
	I did a poor job of failure checking however my cleanup function will always occur at the
	exit of the program therefore it will tell you that the program exited and that the temp
	directory was cleaned up.
	
reddit.py

1. How you parsed command line arguments.
	I used a while loop that looked at the length of the arguments passed into it. If the
	argument started with a - then an if statement was entered to determine which global
	variable needed to be changed then if there was anything after the first argument the 
	search term was determined. 

2. How you fetched the JSON data and iterated over the articles.
	I got the JSON data using the command r.json(). I iterated over the articles using a
	while loop that looked until it hit the total number of articles and reached the LIMIt.
	
3. How you filtered each article based on the FIELD and REGEX.
	I filtered the articles based on weather or not there was a search term or not. If there
	was a search term 

4. How you generated the shortened URL.
	I used request.get and short url a variable I had made previously that added parameters 
	to the link. 




