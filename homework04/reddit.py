#!/usr/bin/env python2.7

import atexit
import os
import re
import sys
import tempfile
import requests

#Global variables

FIELD = 'title'
LIMIT = 10
SUBREDDIT = 'linux'

#Functions

def usage(status=0):
	print '''Usage: reddit.py [ -f FIELD -s SUBREDDIT ] regex
	-f FIELD        Which field to search (default:title)-
	-n LIMIT        Limit number of articles to report (default: 10)
	-s SUBREDDIT    Which subreddit to search (default: linux)'''.format(os.path.basename(sys.argv[0]))
	sys.exit(status)

# Parse command line options

args = sys.argv[1:]

while len(args) and args[0].startswith('-') and len(args[0]) > 1:
	arg = args.pop(0)
	if arg == '-f':
		FIELD = args.pop(0)   
	elif arg == '-n':
		LIMIT = int(args.pop(0))
	elif arg == '-s':
		SUBREDDIT = args.pop(0)
	elif arg == '-h':
		usage(0)
	else:
		print "That flag is not recognized"
		usage(1)

searchterm = ''
if len(args) == 1:
	searchterm = args[0]
elif len(args) != 0:
	usage(1)

#Main execution
try:
	url = "https://www.reddit.com/r/" + SUBREDDIT + "/.json"
	
	#Extract json data
	headers = {'user-agent': 'reddit-{}'.format(os.environ['USER'])}
	r = requests.get(url,headers=headers)
	textdata = r.json()
	datalist = textdata['data']['children']
	shorturl = "http://is.gd/create.php"
	count = 0	
	
	test = datalist[0]['data'][FIELD]
	

	numArticles= 0
	totalArticles = 25
	counterA = 0

	while numArticles < LIMIT  and counterA < totalArticles :
		if searchterm != '':					
			if re.search(searchterm,datalist[counterA]['data'][FIELD]): 
				count = count + 1
				print str(count) + '. Title:   ' + datalist[counterA]['data']['title']							
				print '   Author:  ' + datalist[counterA]['data']['author']
				link = datalist[counterA]['data']['url']
				print '   Link:    ' + link
				parameters = {'format':'json','url':link}				
				ru = requests.get(shorturl,params=parameters)
				mydict = ru.json()
				print '   Short:   ' + mydict['shorturl']
				print ' '
				numArticles = numArticles+1 
						
		else: 
			count = count + 1
			print str(count) + '. Title:   ' + datalist[counterA]['data']['title']
			print '   Author:  ' + datalist[counterA]['data']['author']
			link = datalist[counterA]['data']['url']
			print '   Link:    ' + link
			parameters = {'format':'json','url':link}
			ru = requests.get(shorturl,params=parameters)
			mydict = ru.json()
			print '   Short:   ' + mydict['shorturl']
			print ' '
			numArticles = numArticles+1
			
		counterA= counterA +1
except Exception as e:
	print e
	print('Error found: Program ended')
	sys.exit(0)
