#!/usr/bin/env python2.7

import atexit
import os
import re
import shutil
import sys
import tempfile

import requests

# Global variables

REVERSE     = False
DELAY       = 20
STEPSIZE    = 5

# Functions

def usage(status=0):
    print '''Usage: {} [ -r -d DELAY -s STEPSIZE ] netid1 netid2 target
    -r          Blend forward and backward
    -d DELAY    GIF delay between frames (default: {})
    -s STEPSIZE Blending percentage increment (default: {})'''.format(
        os.path.basename(sys.argv[0]), DELAY, STEPSIZE
    )
    sys.exit(status)

# Parse command line options

args = sys.argv[1:]

while len(args) and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    # TODO: Parse command line arguments
    if arg == '-r':
    	REVERSE = True    
    elif arg == '-d':
    	DELAY = int(args.pop(0)) # Remove the first item in the list and convert to int
    elif arg == '-s':
    	STEPSIZE= int(args.pop(0))
    elif arg == '-h':
    	usage()
    else:
    	print "That flag is not recognized"
    	usage()
    	
if len(args) != 3:
    usage(1)

netid1 = args[0]
netid2 = args[1]
target = args[2]

# Main execution

# TODO: Create workspace
tempDirectory = tempfile.mkdtemp()
print "Using workspace{}" .format(tempDirectory)
#os.removedirs(tempDirectory) # Clean up the directory

# TODO: Register cleanup

def clean_up():
	print 'The program has exited'
	shutil.rmtree(tempDirectory)
	print 'Cleaning up workspace {}' .format(tempDirectory)
	
atexit.register(clean_up)

# TODO: Extract portrait URLs

def search_portrait(netid):
	print "Searching for netid: {}" .format(netid)
	URL = requests.get('https://engineering.nd.edu/profiles/{}'.format(netid))
	imageURL= re.findall('<img src="(.*profiles[^"]*)', URL.text)
	return imageURL[0]

id1image= search_portrait(netid1)
id2image= search_portrait(netid2)

# TODO: Download portraits	

def download_file(imageURL , tempDirectory):
	actualImage= requests.get(imageURL)
	with open(tempDirectory, 'wb') as fd:
		for chunk in actualImage.iter_content(chunk_size=128):
			fd.write(chunk)

download_file(id1image,tempDirectory+'/f1')
download_file(id2image,tempDirectory+'/f2') 

# TODO: Generate blended composite images
index = 0
nameString= ''

while index <= 100:
	numstr = str(index)
	mix = numstr + '-blend.gif'
	picture1 = tempDirectory+'/f1' 
	picture2 = tempDirectory+'/f2'
	print "Generating bended image {}".format(mix) 
	os.system('composite -blend {} {} {} {}'.format(index,picture1,picture2,mix))
	nameString += (mix)+ ' '  # the full string of all the image names
	index += STEPSIZE
	

names = nameString.split() #list 
fullList = nameString + ' '.join(names[::-1]) #list

# TODO: Generate final animation

if REVERSE:
	os.system("convert -loop 0 -delay {} {} {}".format(DELAY, fullList  , target)) 

else :
	os.system("convert -loop 0 -delay {} {} {}".format(DELAY, nameString , target)) 
	
print 'Final gif: {}' .format(target) 

