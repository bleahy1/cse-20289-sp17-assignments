## GRADING
### Homework08
##### Deductions

##### Activity 1: Map Library (12 pts)
**Activity 1: Task 1 - Makefile** 

**Activity 1: Task 2 - entry.c**

**Activity 1: Task 3 - fnv.c**

**Activity 1: Task 4 - map.c**

**Activity 1: Task 5 - README.md**

---
##### Activity 2: Word Frequencies (3 pts)
**Activity 2: Task 1 - freq.c**

**Activity 2: Task 2 - test_freq.sh**

**Activity 2: Task 3 - benchmark**

**Activity 2: Task 4 - README.md**

---
##### Final Grade: 3/15 -- No submission
---
*Graded by Maggie Thomann*
