/* map.c: separate chaining hash table */
/* Consulted Nohemi and she helped with debugging*/

#include "map.h"

/**
 * Create map data structure.
 * @param   capacity        Number of buckets in the hash table.
 * @param   load_factor     Maximum load factor before resizing.
 * @return  Allocated Map structure.
 */
Map *	        map_create(size_t capacity, double load_factor) {
    Map *newMap = calloc( 1, sizeof(newMap));
    
    if (capacity ==0){
        newMap -> capacity = DEFAULT_CAPACITY;
    }
    else {
        newMap -> capacity = capacity;
    }
    if ( load_factor <=0){
        newMap -> load_factor = DEFAULT_LOAD_FACTOR;
    }
    else{
        newMap -> load_factor = load_factor;
    }
    
    newMap -> size = 0;
    
    newMap -> buckets = calloc(newMap -> capacity, sizeof(Entry));
    
    return newMap;
}

/**
 * Delete map data structure.
 * @param   m               Map data structure.
 * @return  NULL.
 */
Map *	        map_delete(Map *m) {
    bool isRecursive = true;
    
    for( size_t i = 0; i < m -> capacity; i++){
        entry_delete( m -> buckets[i].next, isRecursive);
    }
    
    free(m -> buckets);
    
    free(m);
    return NULL;
}

/**
 * Insert or update entry into map data structure.
 * @param   m               Map data structure.
 * @param   key             Entry's key.
 * @param   value           Entry's value.
 * @param   type            Entry's value's type.
 */
void            map_insert(Map *m, const char *key, const Value value, Type type) {

}

/**
 * Search map data structure by key.
 * @param   m               Map data structure.
 * @param   key             Key of the entry to search for.
 * @param   Pointer to the entry with the specified key (or NULL if not found).
 */
Entry *         map_search(Map *m, const char *key) {

    return NULL;
}

/**
 * Remove entry from map data structure with specified key.
 * @param   m               Map data structure.
 * @param   key             Key of the entry to remove.
 * return   Whether or not the removal was successful.
 */
bool            map_remove(Map *m, const char *key) {

 
    return false;
}

/**
 * Dump all the entries in the map data structure.
 * @param   m               Map data structure.
 * @param   stream          File stream to write to.
 * @param   mode            Dump mode to use.
 */
void		map_dump(Map *m, FILE *stream, const DumpMode mode) {
    
}

/**
 * Resize the map data structure.
 * @param   m               Map data structure.
 * @param   new_capacity    New capacity for the map data structure.
 */
void            map_resize(Map *m, size_t new_capacity) {
    return NULL;
	
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
