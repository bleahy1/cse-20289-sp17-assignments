#!/usr/bin/env python2.7

import os
import re
import requests

text = str(requests.get('http://yld.me/aJt?raw=1').text).rstrip()
text = text.split('\n')

#the grep
regex = r'[0-9]{3}-[0-9]{3}-[0-9]{3}[13579]'
result = set([x for x in text if re.search(regex,x)])

#sort and print results
for x in sorted(result):
        print x

