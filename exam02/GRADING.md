GRADING - Exam 02
=================

- Identification:   1.25 
- Web Scraping:     2
- Generators:       5
- Concurrency:      4
- Translation:      3.5 
- Total:	    15.75

Comments
--------

- 1.0   Translation Q1: Doesn't run
- 0.5   Translation Q2: Use re.findall to extract only the phone number
- 1.0   Translation Q3: Runs wrong command, doesn't print total
