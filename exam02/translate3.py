#!/usr/bin/env python2.7

import os

#get results of 'who' command
x = os.popen('ps aux', 'r')


nums = {}
for line in os.popen('ls -l /etc'):
	nums[line.split()[1]] = nums.get(line.split()[1],0) + 1

for num, freq in sorted(nums.items()):
	print '{}\t{}'.format(freq,num)

'''
def f():
	call = "ps aux | awk '{print $1}' | sort | uniq | wc -l"

	os.system(call)

f() 
'''
