#!/usr/bin/env python2.7

import os
import re
import requests

'''def f():
	call = "cat /etc/passwd | cut -d : -f 5 | grep '[uU]ser' | tr 'A-Z' 'a-z' | sort"

	os.system(call)

f() '''


open('/etc/passwd', ‘r’) as f:
        text = f.readlines()

#cut -d : -f 1
fields = [line.split(':')[5] for line in text]


#the grep -E '^[0-9]{2}$' and uniq
regex = '[uU]ser'
result = set([x for x in fields if re.search(regex,x)])


#print sorted results
for x in sorted(result):
        print x



