Reading 07
Brenna Leahy
bleahy1
==========

Part 1
1. Describe what the read_numbers function does by explaining what is happening on the following line:
	while (i < n && scanf("%d", &numbers[i]) != EOF) {
	
	The read numbers function captures the numbers from the
	input stream and stores them in the numbers array. THe
	while loop is executed when i (the counter) is less than
	n which is the size of the array. The scanf function will
	then read whatever is input in standard in and save it as
	an int in the number array. Because of the %d will format
	the input as an int. THe & sign before numbers[i] will
	give the address of the variable rather than the actual
	variable. THe while loop will end when the end of the
	file is reached(when enter is pressed) due to the != EOF. 

2. Insteading of passing size_t n into the sum_numbers function, could we have used sizeof to get the size of the array as shown below?
	
	THis wouldn't work because the sizeof function would be 
	returning the number of bytes in the array. It would only 
	work if it was as follows 
		for (size_t i = 0; i < (sizeof(numbers)/sizeof(int));
		
		
Part 2
1. Compare how this version of cat parses command line arguments versus the approach used in cat.py from Reading 05. Beyond the obvious differences in syntax, how is the code similar and how is it different?
Be sure to explain the role of argc and argv?

	Argc is the number of arguments passed to a program, whereas argv is an array of the string of arguments. IN the cat.c program the program sets the inputs equal to argind and then goes through a while loop that makes sure argind is less than argc, then to get the flags uses a case statement to compare the second argument using argv[argind][1] to compare that input to all the possible flags. Whereas the cat.py program checks if there is anything passed in by using the command len(args) and then checks then checks anything after the first input that starts with a , -,  using the command startswith then uses an if statement instead of a case statement to make the comparisons.  
	
	
2. Describe how each file is processed by explaining the following code: Be sure to explain what the char *path = argv[argind++] statement accomplishes and what the strcmp comparison checks for.

	This block of code checks for a filename or for standard input. If during the comparison it found a ,-, it ran cat_stream because it is a standard input, but if there wasn't a dash ,-, then it ran cat_file because it was a file. the char *path = argv[argind++] is the code that does this iteration and checks the path. 

	
3. Explain how the cat_stream function works. What are the purposes of fgets and fputs?

	Fgets reads in the next charachter from the stream and fputs writes a
	character to stream. 

4. Explain how the cat_file function works. What is the purpose of the following if statement:Be sure to explain what strerror and errno are.

	strerror returns a pointer to an error code whereas errno is the 
	number of the last error. 










