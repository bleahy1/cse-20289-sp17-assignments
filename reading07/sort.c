/* sort.c */

#include <stdio.h>
#include <stdlib.h>

/* Constants */

#define MAX_NUMBERS (1<<10)

/* Functions */

size_t read_numbers(FILE *stream, int numbers[], size_t n) {
    size_t i = 0;

    while (i < n && scanf("%d", &numbers[i]) != EOF) {
        i++;
    }

    return i;
}

void sort_numbers(int numbers[], size_t n) {
    if (n <= 1) return;
    
    for (size_t i = 0; i < n; i++) {
        for ( size_t k = i; k > 0 && numbers[k] < numbers[k-1]; k--) {
        	int tempNumber = numbers[k];
        	numbers[k] = numbers[k-1];
        	numbers[k-1] = tempNumber;
        }
    }
}


/* Main Execution */

int main(int argc, char *argv[]) {
    int numbers[MAX_NUMBERS];
    int i;
    size_t nsize;
	
    nsize = read_numbers(stdin, numbers, MAX_NUMBERS);
    
    if (nsize < 1){
    	printf("\n");
    	return EXIT_SUCCESS;
    }
    
    sort_numbers(numbers, nsize);
    printf("%d", numbers[0]);
    
    for (i =1 ; i < nsize ; i++)
    	printf("%d", numbers[i]);
    
    
    printf("\n");
        

    return EXIT_SUCCESS;
}







