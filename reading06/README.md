Reading 06
==========

A. What problem is MapReduce trying to solve?
	It is a way to handle very large sets of data. as well as to
	ensure there isn't duplicate sets of data. 

B. Describe the three phases of a typical MapReduce workflow.
	Map: Applies the map function to local data so that only one
	     copy of data is processed and writes the data to temporary
	     storage.
	Shuffle: redistributes data based on output keys so that all data
		   that belongs to one key is located on the same node. 
	Reduce: Worker nodes process each group of data and output data
		   per keyu in parallel. 
	
