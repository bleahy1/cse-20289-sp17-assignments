#!/usr/bin/env python2.7

import sys

print ' '.join([ n for n in [ lists.strip() for lists in sys.stdin ] if int(n)  % 2 == 0 ])
