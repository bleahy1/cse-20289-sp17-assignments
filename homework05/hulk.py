#!/usr/bin/env python2.7

import functools
import hashlib
import itertools
import multiprocessing
import os
import string
import sys

# Constants

ALPHABET    = string.ascii_lowercase + string.digits
ARGUMENTS   = sys.argv[1:]
CORES       = 1
HASHES      = 'hashes.txt'
LENGTH      = 1
PREFIX      = ''

# Functions

def usage(exit_code=0):
    print '''Usage: {} [-a alphabet -c CORES -l LENGTH -p PATH -s HASHES]
    -a ALPHABET Alphabet to use in permutations
    -c CORES    CPU Cores to use
    -l LENGTH   Length of permutations
    -p PREFIX   Prefix for all permutations
    -s HASHES   Path of hashes file'''.format(os.path.basename(sys.argv[0]))
    sys.exit(exit_code)

def md5sum(s): #s is the string done 
    ''' Generate MD5 digest for given string.

    >>> md5sum('abc')
    '900150983cd24fb0d6963f7d28e17f72'

    >>> md5sum('wake me up inside')
    '223a947ce000ce88e263a4357cca2b4b'
    '''
    # TODO: Implement
    	#This works but it says it doesn't
    return hashlib.md5(s).hexdigest()


def permutations(length, alphabet=ALPHABET):
	''' Yield all permutations of alphabet of specified length.

    >>> list(permutations(1, 'ab'))
    ['a', 'b']

    >>> list(permutations(2, 'ab'))
    ['aa', 'ab', 'ba', 'bb']

    >>> list(permutations(1))       # doctest: +ELLIPSIS
    ['a', 'b', ..., '9']

    >>> list(permutations(2))       # doctest: +ELLIPSIS
    ['aa', 'ab', ..., '99']
    '''
	#xs = alphabet
	#length2= length
	if(length == 1):
		for char in alphabet:
			yield char
	else:
		for c in alphabet:
			for rest in permutations(length-1, alphabet):
				yield c + rest
             

def smash(hashes, length, alphabet=ALPHABET, prefix=''):
	''' Return all password permutations of specified length that are in hashes

    >>> smash([md5sum('ab')], 2)
    ['ab']

    >>> smash([md5sum('abc')], 2, prefix='a')
    ['abc']

    >>> smash(map(md5sum, 'abc'), 1, 'abc')
    ['a', 'b', 'c']
    '''
    # TODO: Implement with list or generator comprehensions +
	
	makeList = [prefix + i for i in permutations ( length, alphabet)]
	
	matchList = [same for same in makeList if md5sum(same) in hashes]

	return matchList

# Main Execution

if __name__ == '__main__':
    # TODO: Parse command line arguments
    #ARGUMENTS = sys.argv[1:]
    
	while len(ARGUMENTS) and ARGUMENTS[0].startswith('-') and len(ARGUMENTS[0]) > 1:
		arg = ARGUMENTS.pop(0)
		if arg == '-h':
			usage(0)
		elif arg == '-a':
			ALPHABET = ARGUMENTS.pop(0) 
		elif arg == '-c':
			CORES = int(ARGUMENTS.pop(0))
		elif arg == '-l':
			LENGTH= int(ARGUMENTS.pop(0))
		elif arg == '-p':
			PREFIX = ARGUMENTS.pop(0)
		elif arg == '-s':
			HASHES = ARGUMENTS.pop(0)
		else:
			print "That flag is not recognized"
			usage(1)
        	

		
    # TODO: Load hashes set
	hashes = set ([i.strip() for i in open(HASHES)])
	
    # TODO: Execute smash function to get passwords
	if(CORES> 1 and LENGTH >1):
		makeList = []

		subsmash = functools.partial(smash, hashes, LENGTH - (LENGTH/2), ALPHABET)
		pool = multiprocessing.Pool(CORES)
		passwords = itertools.chain.from_iterable(pool.imap(subsmash, (PREFIX + i for i in permutations(LENGTH/2, ALPHABET))))
	else:
		passwords = smash(hashes, LENGTH , ALPHABET, PREFIX)
		
		
# TODO: Print password
	for words in passwords:
		print words
		
		
