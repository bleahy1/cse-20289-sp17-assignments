#!/usr/bin/env python2.7


import sys
#import fileinput
import string

counter = 0



for line in sys.stdin:
	counter = counter + 1
	for word in line.strip().split():
		allow = set(string.ascii_lowercase + string.digits + '-')
		word = ''.join(x for x in word.lower() if x in allow)
		print '{}\t{}'.format(word, counter)
