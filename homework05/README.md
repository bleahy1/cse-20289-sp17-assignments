Homework 05
===========
1. Describe how you implemented the hulk.py script. In particular, briefly discuss:

	a. How you generated all the candidate password combinations.
		THIS WAS DONE WSITH THE MD5SUM FUNCTION. THAT RETURNED THE HASH to the specific input. 

	b. How you filtered the candidates to only contain valid passwords.
		The smashes function filtered the passwords to include the only valid passwords.  

	c. How you handled processing on multiple cores.
		I checked if the multiple core flag was used, if it was then i used imap to use more than one cores and multiprocessing.Pool(CORES). 

	d. How you verified that your code works properly.
		I ran all the tests code provided and hoped that when I ran the hulk smash for a length of 8 before going to bed that it actually finished running.

	e. Complete the following table for passwords of length 6:	

2.       Processes 			Time
			1	651.83user 43.02system 22:09.24elapsed Cancelled  
			2	6192.06user 141.33system 47:36.30elapsed 
			4	6047.41user 147.62system 23:16.41elapsed
			8	6315.57user 150.01system 12:24.53elapsed 
			16	6319.13user 145.78system 10:34.33elapsed

	a. How does the number of processes utilized affect the amount of time required to crack passwords?
	More processes means shorter time because the cores work together to crack the passwords. Although 8 and 16 processes seem to be roughly the same because after 8 processes there are only so many passswords to be craked after increaseing the processes it can only increase the time so much. That is also because the machine I used only had 12 cores.  
	

3. From your experience in this project and recalling your Discrete Math knowledge, what would make a password more difficult to brute-force: more complex alphabet or longer password? Explain.
	A longer pass word. Because there are more locations to check making it more complex. 


1. How does the iv_map.py script work.

	a. How does it keep track of line numbers.
		The variable counter keeps track of the line numbers. and every time the function is executed it adds one to counter. 

	b. How does it remove undesirable characters.
		allow checks the stdin for the allowed characters if the word is acceptable its added to the list  and printed out with the counter representing the line number. 

2. How does the iv_reduce.py script work.

	a. How does it aggregate the results for each word.
		Removes all the numbers and spaces and then adds all of the words to a list to act as a dictionary. 

	b. How does it output the results in the correct format.
		The for loop uses .sort to to print everything with the correct format. 
	
	
	
