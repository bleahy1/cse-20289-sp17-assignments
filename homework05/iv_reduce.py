#!/usr/bin/env python2.7

import sys
import string

counts = {}

for line in sys.stdin:
    k, v  = line.split('\t', 1)
    counts[k] = counts.get(k, '') + ' ' + v.strip()
    counts[k]= ' '.join(set(counts[k].split()))

for k, v in sorted(counts.items()):
	v = list(v.split())
	v = [int(x) for x in v]
	v.sort()
	print '{}\t{}'.format(k, ' '.join([str(c) for c in v]))
