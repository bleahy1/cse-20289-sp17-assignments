#include <stdio.h>
#include <dirent.h>
#include <sys/stat.h>


int main ( int argc, char *argv[]){
	DIR *d;
	struct dirent *directory;
	struct stat buf;
	int size = 0;
	
	d = opendir(".");
	
	if (d) {
		while ((directory = readdir(d)) != NULL ) {
			stat(directory -> d_name, &buf) ;
			size = buf.st_size;
			printf(" %s %d \n " , directory -> d_name, size);
			
		}
		closedir(d);
	}
	return 0;
}
