Reading 10
==========

1. Identify which system calls you would use to perform the 
   following actions by briefly listing example C code:

		a. Print out the error message associated with a failed system call.
				PERROR(const char * str);

		b. Truncate a file.
				TRUNCATE(path to file, length to truncate)

		c. Move to the 10th byte of a file.
				LSEEK(fd, 10, SEEK_CURR);

		d. Check if a path is a directory.
				STAT = it checks the path

		e. Create a copy of the current process.
				fork();

		f. Replace the code in the current process with another program.
				EXEC or EXECVP or EXECL

		g. Tell a process to terminate.
				-kill SIGINT (PID)- terminates nicely
				-kill SIGQUIT (PID)- terminates harshly

		h. Receive the exit status of a child process.
				WEXITSTATUS(status)
