#!/bin/sh

PATH=/afs/nd.edu/user15/pbui/pub/bin:$PATH

#if [ "$#" -eq "0" ]; then # not printing out that README.md exists in the directory 
 #   echo "ERROR WAS ENCOUNTERED"  # print the value for the $1 variable 
#	exit 1
#fi


trap end_prog 1 2 15 

end_prog(){

	cowsay "Fine be mean, I didn't want to talk to you anyway!"
	exit 1
}

cowsay "Hello there human, please talk to me ..."
INPUT="" 

while [ -z $INPUT ]

do
	echo "come on!!! Ask me a question?"
	read -a INPUT
done


shuf -n 1 << 'EOF' | cowsay 
It is certain
It is decidedly so
Without a doubt
Yes, definitely
You may rely on it
As I see it, yes
Most likely
Outlook good
Yes
Signs point to yes
Reply hazy try again
Ask again later
Better not tell you now
Cannot predict now
Concentrate and ask again
Dont count on it
My reply is no
My sources say no
Outlook not so good
Very doubtful
EOF
