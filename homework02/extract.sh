#!/bin/bash

if [ "$#" -eq "0" ]; then 
    echo "Usage: extract.sh archive1 archive2..."  
	exit 1
fi

while [ "$#" -gt "0" ]
do
	case $1 in
		(*.tgz | *.tar.gz)     # gzip
		tar -xzf $1 ;;
		
		(*.tbz | *.tar.bz2)      # bzip2
		tar -xjf $1 ;;
		
		(*.txz | *.tar.xz)        # xz
		tar -xJf $1 ;;
		
		(*.zip | *.jar)           # zip
		unzip $1 ;;	
	
	esac
	shift
done


