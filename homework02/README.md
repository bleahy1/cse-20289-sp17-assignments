Homework 02
Brenna Leahy
bleahy1
===========
### Activity 1: Saving the World 

1. How you checked and handled having no arguments.
	The if statement at the begining checks for no arguments. and if there are no arguments it prints the usage statement. 
	
2. How you determined which command to use for each argument.
	I searched what flag do I need to extract a .tar.gz file on ask ubuntu. Then looked around the threads for different flags and tried them until the test script was successful.  
	
3. What was the most challenging aspect of writing this script and how you overcame this issue.
	I had a lot of trouble with the spacing as well as I forgot the shift for a while until I referenced reading 2 exist.sh and realized that is what I forgot.

### Activity 2: Predicting the Future

1. How you displayed random messages to the user.
	shuf -n 1 << 'EOF' | cowsay
	Shuf randomly selected one of the multiple messages. -n was to read the entire line. And then read for the end of the function and sent it with cowsay so the cow shows up. 
	
2. How you handled signals.
	Trap handles and catches the signals. Whenever 1 ( sighip) ,3 (sigquit),or 15 (sigterm)comes up it yells at the user saying, "Fine be mean, I didn't want to talk to you anyway!" by going into the function end_prog. 

3. How you read input from the user.
	Read and input were used to get the input from the user. The input variable was set to be nothing so if there was something there the read command would be called and then cowsay would respond with an answer. 
	
4. What was the most challenging aspect of writing this script and how you overcame this issue.
	For the longest time it was continuously asking for a question from the user without adding the cowsay. I fixed this because I had the path wrong. Also no spaces when initializing variables is really hard to get used too. 

### Activity 3: Meeting with Oracle

1. My first step was to scan `xavier.h4x0r.space` for a HTTP port:

        $ command:  nmap -v -Pn xavier.h4x0r.space
        Output:
   		$ nmap -v -Pn xavier.h4x0r.space

				Starting Nmap 5.51 ( http://nmap.org ) at 2017-02-03 15:54 EST
				Initiating Parallel DNS resolution of 1 host. at 15:54
				Completed Parallel DNS resolution of 1 host. at 15:54, 0.00s elapsed
				Initiating Connect Scan at 15:54
				Scanning xavier.h4x0r.space (129.74.160.130) [1000 ports]
				Discovered open port 8888/tcp on 129.74.160.130
				Discovered open port 22/tcp on 129.74.160.130
				Discovered open port 9876/tcp on 129.74.160.130
				Connect Scan Timing: About 46.55% done; ETC: 15:55 (0:00:36 remaining)
				Discovered open port 9111/tcp on 129.74.160.130
				Completed Connect Scan at 15:55, 65.55s elapsed (1000 total ports)
				Nmap scan report for xavier.h4x0r.space (129.74.160.130)
				Host is up (0.55s latency).
				Not shown: 946 filtered ports, 50 closed ports
				PORT     STATE SERVICE
				22/tcp   open  ssh
				8888/tcp open  sun-answerbook
				9111/tcp open  DragonIDSConsole
				9876/tcp open  sd

				Read data files from: /usr/share/nmap
				Nmap done: 1 IP address (1 host up) scanned in 65.58 seconds        
        
        

    As can be seen, there are `X` ports in the `9000` - `9999` range.

2. Next, I tried to access the HTTP server:

        $ command: curl xavier.h4x0r.space:9876
        Output:
         ________________________________________
		/ Halt! Who goes there?                  \
		|                                        |
		| If you seek the ORACLE, you must come  |
		| back and _request_ the DOORMAN at      |
		| /{NETID}/{PASSCODE}!                   |
		|                                        |
		| To retrieve your PASSCODE you must     |
		| first _find_ your LOCKBOX which is     |
		| located somewhere in                   |
		| ~pbui/pub/oracle/lockboxes.            |
		|                                        |
		| Once the LOCKBOX has been located, you |
		| must use your hacking skills to        |
		| _bruteforce_ the LOCKBOX program until |
		| it reveals the passcode!               |
		|                                        |
		\ Good luck!                             /
		 ----------------------------------------
		  \
		   \
			   ___
			 {~._.~}
			  ( Y )
			 ()~*~()
			 (_)-(_)


the next thing I did was try to find my lock box the way i did this frst was cd into each individual directory in ~pbui/pub/oracle/lockboxes until I found the one that said my name. which was,

cd 8f66e26a/3bbcc92d/900ce108/c454d029/

After that I went to office hours and figured out there was a find command fo from there I did the following.  

find -name "bleahy1*"

in bash:
for i in $(strings bleahy1.lockbox ) ; do echo $i ; done ;

for i in $(strings bleahy1.lockbox ) ; do bleahy1.lockbox  $i ; done ;

I then found my password to be the following, 

fa59b5c0a39e5ed36510a16a302d30d1

Since the oracle had already been taken down by midterms, Pierce walked me through what I would have done with the password but I was unable to actuall talk to the oracle. 



