Homework 06 - Grading
====================

**Score**: 11.75/15

Deductions
----------
stringutils.c
-.25 used array access on strings instead of pointers in string_lowercase
-.25 used array access on strings instead of pointers in string_uppercase
-.25 string_chomp used array access on string instead of pointers

README (Activity 1)
-.5 missing time and space complexity for #1
-.5 missing time and space complexity for #2
-.5 missing time and space complexity for #3

Makefile
-.25 Makefile is partially incorrect. Missing test-translate. translate-static also depends on libstringutils.a and translate dynamic also depends on libstringutils.so

translate.c
-1.25 for 5 failed tests

README (Activity 2)
-.25 question 1 is missing explanations of implemenatation of translation, filters, and the bitmask
-.25 you said executing translate-static does not work but it does. also to run translate-dynamic you need to add the current directory to LD_LIBRARY_PATH

Comments
--------
- for the string_lowercase and string_uppercase functions you could have used the tolower and toupper functions provided by ctype.h
- for string_reverse you could have used the string_reverse_range function you wrote just before it
