Homework 06
===========


1. Describe how your string_reverse_words function works and analyze its time complexity and space complexity in terms of Big-O notation.
	String Reverse function steps through the string and sets the 
	end equal to the string. 	
	Space Complexity:______________
	Time Complexity:_______________

2. Describe how your string_translate function works and analyze its time complexity and space complexity in terms of Big-O notation.
	First it checks that there is a string to translate from and 
	to. Once it has done this it starts to walk through the pointer
	to the c string. While c is less than the from string and steps
	through until all of the changes have been made. 
	Space Complexity:______________
	Time Complexity:_______________

3.  Describe how your string_to_integer function works and analyze its time complexity and space complexity in terms of Big-O notation.
	The string to integer function works by first making all of the
	letters capitals. Then it starts dealing with the integers 0-9.
	If they are a charachter 0-9 the 0 charachter is subtracted.	
	And then in order to deal with the letters anything other than
	a number charachter it takes the number and then subtracts the
	a character and adds 10 to it to put it in the correct range.
	It then did math to convert the integer value. 
	Space Complexity:______________
	Time Complexity:_______________

4. What is the difference between a shared library such as libstringutils.so and a static library such as libstringutils.a?
	A static library will be linked inside the executable. It is
	used ar compile time and the contents of the library become a
	part of the consuming executable on compile. Whereas a dynamic
	library is loaded at runtime and not compiled into the
	executable.  

		Comparing the sizes of libstringutils.a and libstringutils.so, which
		one is larger? Why?
		libstringutils.a = 12k
		libstringutils.so = 13k 
		LIBSTRINGUTILS.SO IS LARGER. 

1. Describe how the translate.c program works. In particular,
explain:

	How you implemented command line parsing.
		I used the variable argind and argc and when the argind variable was less than the argc as well as the length of the argv of argind was greater than zero and it wasn't a - theb it read in the letter that was passed in to the command line ans a switch statement was used to determine what command to run. 

	How you implemented the translation.

	How you implemented the different post processing filters.

	Be sure to describe how you used and handled the mode bitmask.

2. What is the difference between a static executable such as
translate-static and a dynamic executable such as translate-dynamic?

	Comparing the sizes of translate-static and translate-dynamic,
	which one is larger? Why?
		Translate static = 975K
		translate dynamic = 13k
		Translate static is much much larger. Static is done at compile time whereas dynamic is done by the operating system,many of the functions used in translate dynamic reside in an external library file which is referenced by the operating system when the compiler tells the software to find the function. 

	When trying to execute translate-static, does it work? If not,
	explain what you had to do in order for it to actually run.
		No it doesn't run. It is used by the other programs when they are ran.SO run the other programs. 
	
	When trying to execute translate-dynamic, does it work? If not, 
	explain what you had to do in order for it to actually run.
		No it doesn't run. 



