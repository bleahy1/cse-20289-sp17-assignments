/* stringutils.c: String Utilities */

#include "stringutils.h"

#include <ctype.h>
#include <string.h>
#include <math.h>

/**
 * Convert all characters in string to lowercase.
 * @param   s       String to convert
 * @return          Pointer to beginning of modified string
 **/
char *	string_lowercase(char *s) {

    int i = 0;
    char* newStr = strdup(s);
    while(newStr[i]){
        if( newStr[i] >= 65 && newStr[i] <= 90)
            newStr[i] += 32;//newStr[i] - 32;
        i++;
    }
    return newStr;
}

/**
 * Convert all characters in string to uppercase.
 * @param   s       String to convert
 * @return          Pointer to beginning of modified string
 **/
char *	string_uppercase(char *s) {
    int i = 0;
    char *newStr = strdup(s);
    while(newStr[i]){
        if( newStr[i] >= 97 && newStr[i] <= 122){
            newStr[i] -= 32;//newStr[i] - 32;
        }
        i++;
    }
    return newStr;
}

/**
 * Returns whether or not the 's' string starts with given 't' string.
 * @param   s       String to search within
 * @param   t       String to check for
 * @return          Whether or not 's' starts with 't'
 **/
bool	string_startswith(char *s, char *t) {
    char *newStrS = strdup(s);
    char *newStrT = strdup(t);
    bool answer = true; 
    
    int len = strlen(newStrT);
    for (int i = 0; i < len ; i ++){

        if (*(newStrS + i) == *(newStrT + i)){
            answer = true;
            i++;
        }
        else if(*(newStrS + i) != *(newStrT + i)){
            answer = false;
            break;
        }
    }
    return answer;
        
}

/**
 * Returns whether or not the 's' string ends with given 't' string.
 * @param   s       String to search within
 * @param   t       String to check for
 * @return          Whether or not 's' ends with 't'
 **/
bool	string_endswith(char *s, char *t) {
    char *newStrS=s;
    char *newStrT=t;
    bool answer = true; 
    int lenT = strlen(newStrT);
    int lenS = strlen(newStrS);
    int startSpot = lenS - lenT;


    char* sWord;
    char* tWord;
    
    sWord = s + startSpot;
    tWord = t;    

    for (tWord = t; *tWord; tWord++, sWord++){ 
        if(*tWord != *sWord){
            answer = false;
        }
    }

    return answer;
}

/**
 * Removes trailing newline (if present).
 * @param   s       String to modify
 * @return          Pointer to beginning of modified string
 **/
char *	string_chomp(char *s) {
    int lengthString;

    if( (lengthString = strlen(s)) > 0 ) {
        if( s[lengthString-1] == '\n'){
            s[lengthString-1] = '\0';
        }
    }

    return s;
}

/**
 * Removes whitespace from front and back of string (if present).
 * @param   s       String to modify
 * @return          Pointer to beginning of modified string
 **/
char *	string_strip(char *s) {
    char *end;

    while(isspace ((unsigned char) *s)) s ++;

    if(*s == 0){
        return s;
    }

    end = s + strlen(s) -1;

    while(end > s && isspace ((unsigned char) *end )) end --;

    *(end +1) = 0;

    return s;
}

/**
 * Reverses a string given the provided from and to pointers.
 * @param   from    Beginning of string
 * @param   to      End of string
 * @return          Pointer to beginning of modified string
 **/
static char *	string_reverse_range(char *from, char *to) {
    char switchChar; 
    while (from < to)
    {   
        switchChar = *from; 
        *from = *to; 
        *to = switchChar;  
        from++; 
        to--;      
    }
    return from;
}

/**
 * Reverses a string.
 * @param   s       String to reverse
 * @return       
   Pointer to beginning of modified string
 **/
char *	string_reverse(char *s) {
    char *string = s;
    char *end = string + strlen(string) -1;
    while(end > string){
        char i = *end;
        *end -- = *string;
        *string++ = i;
    }
    return s;
}

/**
 * Reverses all words in a string.
 * @param   s       String with words to reverse
 * @return          Pointer to beginning of modified string
 **/
char *	string_reverse_words(char *s) {
    s = string_reverse(s);
    char *c;
    c=s;
    char *f;
    
    
    while (*c){
        while ( *c && isspace(*c)){
            c ++;
        }
        f= c;
        while ( *c && !isspace(*c)){
            c++;
        }
        string_reverse_range(f, c-1);
    }   
    return s; 
}

/**
 * Replaces all instances of 'from' in 's' with corresponding values in 'to'.
 * @param   s       String to translate
 * @param   from    String with letter to replace
 * @param   to      String with corresponding replacment values
 * @return          Pointer to beginning of modified string
 **/
char *	string_translate(char *s, char *from, char *to) {
    int fromLen = strlen(from);
    int toLen = strlen(to);
    int stringLen = strlen(s);

    unsigned char *c = (unsigned char *)s;

    if (fromLen ==0)
        return s;
    if (toLen ==0)
        return s;
    if (stringLen ==0)
        return s;

    while(*c) { 
        for (int i = 0; i < fromLen ; i ++){
            if(*c == *( from + i))
                *c = *(to + i);
        }
        c++;
    }
    return s;
}

/**
 * Converts given string into an integer.
 * @param   s       String to convert
 * @param   base    Integer base
 * @return          Converted integer value
 **/
int	string_to_integer(char *s, int base) {
    s= string_uppercase(s);
    int length = strlen(s);
    int num;
    int power = 1;
    int place = 1;
    int total = 0;

    for (int i = length -1; i >= 0; i --) {
        num = *(s+i);
        if (num <= '9' && num >= '0' ){ 
            num = num - '0';
        }
        else{
            num = num - 'A' + 10; // # - a + 10 
        }
        total = num * place + total;
        place = pow(base, power);
        power++;
    }
    return total;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
