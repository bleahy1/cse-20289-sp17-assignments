/* translate.c: string translator */

#include "stringutils.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char *PROGRAM_NAME = NULL;

/* Define Modes */
enum {
    STRIP = 1<<1,
    LOWER = 1<<2,
    UPPER = 1<<3,
    REVERSE = 1<<4,
    REVERSEW = 1<<5,
};


/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s SOURCE TARGET\n\n", PROGRAM_NAME);
    fprintf(stderr, "Post Translation filters:\n\n");
    fprintf(stderr, "   -s     Strip whitespace\n");
    fprintf(stderr, "   -r     Reverse line\n");
    fprintf(stderr, "   -w     Reverse words in line\n");
    fprintf(stderr, "   -l     Convert to lowercase\n");
    fprintf(stderr, "   -u     Convert to uppercase\n");
    exit(status);
}

void translate_stream(FILE *stream, char *source, char *target, int mode) {
    /* TODO */
    char buffer[BUFSIZ];

    while (fgets(buffer, BUFSIZ, stream)){
        string_chomp(buffer);
        
        if(source != NULL && target != NULL) {
            string_translate(buffer, source, target);
        }

        if (mode & STRIP) {
            string_strip(buffer);
        }

        if (mode & LOWER) {
            string_lowercase(buffer);
        }

        if (mode & UPPER) {
            string_uppercase(buffer);
        }
    
        if (mode & REVERSE) {
            string_reverse(buffer);
        }

        if (mode & REVERSEW) {
            string_reverse_words(buffer);
        }

        fputs(buffer, stdout);
        fputc('\n', stdout);
    }
}

/* Main Execution */

int main(int argc, char *argv[]) {
    /* Parse command line arguments */

    /* Translate Stream */

    int argind = 1;

    int mode = 0;
    char *source, *target;
    PROGRAM_NAME = argv[0];

    while(argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-') {
        char *arg = argv[argind++];
        switch(arg[1]) {
            case 'h':
                usage(0);
                break;
            case 's':
                mode |= STRIP; //*** Doesn't pass test 
                break;
            case 'l':
                mode |= LOWER; //*** Doesn't pass test or valgrind 
                break;
            case 'u':
                mode |= UPPER; //*** doesn't pass test or valgrind
                break;
            case 'r':
                mode |= REVERSE;
                break;
            case 'w':
                mode |= REVERSEW;
                break;
            default:
                usage(1);
                break;
        
         }
    }
        //translate_stream(FILE *stream, char *source, char *target, int mode) 
        
    if (argind  <= argc - 2) {
        source = argv[argind++];
        target = argv[argind++];
    }

    else {
        source = NULL;
        target = NULL;
    }

    translate_stream(stdin, source, target, mode);

    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
