Reading 08
Brenna Leahy
bleahy1
==========

1.  1= 4 bytes
	2= 20 bytes
	3= 8 bytes
	4= 16 bytes
	5= 8 bytes
	6= 8 bytes 
	7= 8 bytes
	8= 8 bytes
	
	
2. Describe what the memory error was in Task 3 and how you fixed it.
	The error was in bool duplicates(). It was the line of code that was 
		int *randoms = malloc(n);
	the code was changed to the following:
		int *randoms = malloc(n * sizeof(int));

3. Describe what the memory leak was in Task 4 and how you fixed it.
	By putting in the randoms[i] = rand() % 1000, you limit the number of random numbers that are possible, so there are bound to be more duplicates. WIth more duplicates there is less space being freed up, so to fix this, a free(randoms) was placed before the return true. 
