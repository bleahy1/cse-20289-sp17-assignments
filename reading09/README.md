Reading 09
Brenna Leahy 
bleahy1
==========

1. Given the C code below:
	a). What is the size of s and what is the size of u?
		size of s = 5 bytes 
		size of u = 4 bytes
		This is because memory for a union is the memory required for the largest
		element of a union. Whereas a struct is the total memory required. 

	b). What is the difference between a union and a struct?
		In a struct all members can be accessed at any time, whereas in a union
		only one of the members can be accessed at a time, every other member
		contains garbage values.

2. Given the C code below (message.c):
	a). What message does this program output?
		fate is inexorable!

	b). Use the DUMP_BITS macro to explore this program and then explain how it
	works (that is, how does setting the values above in the struct Thing t
	object yield the final message).
		____________________________________

	c). What does this program tell you about types, memory, and how data is
	represented in C? What are we really doing when we cast values in C?
		____________________________________


3. A hash table can be classified by how it handles collisions:

	a). What exactly is a collision?
		A collision is where two different inputs map to the same output. This
		can happen frequently with hash tables because they are used to 
		compress large sets of data to smaller more manageable sets of data. 

	b). How are collisions handled in a hash table that uses separate chaining?
		In this way, each bucket represents a list. So therefore each list
		has the same index but can be filled with multiple different sets
		of data. 

	c). How are collisions handled in a hash table that uses open addressing?
		Open addressing involves a way to only place one data point into each
		bucket by placing the second data point that belongs in an already
		occupied bucket in an unoccupied bucket. THis could be the closest
		bucket for example. With open addressing no bucket has more than one 
		data point. 
		


4. Starting with an empty hash table:
	
	a). Using separate chaining and using the hash function h(x) = x % 10, show
		the result of inserting the following values into the table:
		7, 3, 2, 78, 56, 72, 79, 68, 13, 14

		Bucket	Value
		0					
		1	
		2			2, 72
		3			3, 13
		4			14
		5			
		6			56,
		7			7,
		8			78, 68,
		9			79,


	b). Using open addressing (and linear probing with an interval of 1), and
		using the hash function h(x) = x % 10, show the result of inserting the
		following values into the table:
		7, 3, 2, 78, 56, 72, 79, 68, 13, 14
		7, 3, 2, 8, 6, 2, 9, 8, 3, 4

		Bucket	Value
		0			68			
		1			14	
		2			2	
		3			3		
		4			72	
		5			13	
		6			56	
		7			7		
		8			78	
		9			79	


